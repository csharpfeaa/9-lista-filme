﻿namespace ListaFilme
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstWatchlist = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lstWatched = new System.Windows.Forms.ListBox();
            this.btnMoveToWatched = new System.Windows.Forms.Button();
            this.btnMoveToWatchlist = new System.Windows.Forms.Button();
            this.txtSearchMovie = new System.Windows.Forms.TextBox();
            this.btnSearchMovie = new System.Windows.Forms.Button();
            this.btnResetList = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lstWatchlist
            // 
            this.lstWatchlist.FormattingEnabled = true;
            this.lstWatchlist.Location = new System.Drawing.Point(12, 48);
            this.lstWatchlist.Name = "lstWatchlist";
            this.lstWatchlist.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.lstWatchlist.Size = new System.Drawing.Size(182, 225);
            this.lstWatchlist.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(56, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Watchlist";
            // 
            // lstWatched
            // 
            this.lstWatched.FormattingEnabled = true;
            this.lstWatched.Location = new System.Drawing.Point(327, 48);
            this.lstWatched.Name = "lstWatched";
            this.lstWatched.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.lstWatched.Size = new System.Drawing.Size(182, 225);
            this.lstWatched.TabIndex = 2;
            // 
            // btnMoveToWatched
            // 
            this.btnMoveToWatched.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnMoveToWatched.Location = new System.Drawing.Point(223, 89);
            this.btnMoveToWatched.Name = "btnMoveToWatched";
            this.btnMoveToWatched.Size = new System.Drawing.Size(75, 64);
            this.btnMoveToWatched.TabIndex = 3;
            this.btnMoveToWatched.Text = ">";
            this.btnMoveToWatched.UseVisualStyleBackColor = true;
            this.btnMoveToWatched.Click += new System.EventHandler(this.btnMoveToWatched_Click);
            // 
            // btnMoveToWatchlist
            // 
            this.btnMoveToWatchlist.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnMoveToWatchlist.Location = new System.Drawing.Point(223, 159);
            this.btnMoveToWatchlist.Name = "btnMoveToWatchlist";
            this.btnMoveToWatchlist.Size = new System.Drawing.Size(75, 64);
            this.btnMoveToWatchlist.TabIndex = 4;
            this.btnMoveToWatchlist.Text = "<";
            this.btnMoveToWatchlist.UseVisualStyleBackColor = true;
            // 
            // txtSearchMovie
            // 
            this.txtSearchMovie.Location = new System.Drawing.Point(12, 298);
            this.txtSearchMovie.Name = "txtSearchMovie";
            this.txtSearchMovie.Size = new System.Drawing.Size(181, 20);
            this.txtSearchMovie.TabIndex = 5;
            this.txtSearchMovie.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.searchKeyPress);
            // 
            // btnSearchMovie
            // 
            this.btnSearchMovie.Location = new System.Drawing.Point(12, 325);
            this.btnSearchMovie.Name = "btnSearchMovie";
            this.btnSearchMovie.Size = new System.Drawing.Size(181, 23);
            this.btnSearchMovie.TabIndex = 6;
            this.btnSearchMovie.Text = "Cauta film";
            this.btnSearchMovie.UseVisualStyleBackColor = true;
            this.btnSearchMovie.Click += new System.EventHandler(this.btnSearchMovie_Click);
            // 
            // btnResetList
            // 
            this.btnResetList.Location = new System.Drawing.Point(12, 354);
            this.btnResetList.Name = "btnResetList";
            this.btnResetList.Size = new System.Drawing.Size(181, 23);
            this.btnResetList.TabIndex = 7;
            this.btnResetList.Text = "Reset";
            this.btnResetList.UseVisualStyleBackColor = true;
            this.btnResetList.Click += new System.EventHandler(this.btnResetList_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(521, 405);
            this.Controls.Add(this.btnResetList);
            this.Controls.Add(this.btnSearchMovie);
            this.Controls.Add(this.txtSearchMovie);
            this.Controls.Add(this.btnMoveToWatchlist);
            this.Controls.Add(this.btnMoveToWatched);
            this.Controls.Add(this.lstWatched);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lstWatchlist);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lstWatchlist;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox lstWatched;
        private System.Windows.Forms.Button btnMoveToWatched;
        private System.Windows.Forms.Button btnMoveToWatchlist;
        private System.Windows.Forms.TextBox txtSearchMovie;
        private System.Windows.Forms.Button btnSearchMovie;
        private System.Windows.Forms.Button btnResetList;
    }
}

