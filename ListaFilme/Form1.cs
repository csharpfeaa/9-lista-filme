﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ListaFilme
{
    public partial class Form1 : Form
    {
        // filmele din watchlist
        string[] movies = { "The Fountain", "The Tsunami and the Cherry Blossom", "The Redemption of General Butt Naked", "Ghost Dog: The Way of the Samurai", "Samsara", "The Book of Eli", "The Dark Knight", "God Is the Bigger Elvis", "This Is the End", "Living in Emergency", "Fight Club", "The Cove", "The Matrix", "My Dinner with Andre", "American Beauty", "The King's Speech", "If a Tree Falls: A Story of the Earth Liberation Front", "Les Misérables", "Baraka", "50/50", "Network", "Searching for Sugar Man", "Exporting Raymond", "Man on Fire", "Office Space", "Beasts of the Southern Wild", "The Godfather", "Restrepo", "The Pursuit of Happyness", "Dr. Strangelove or: How I Learned to Stop Worrying and Love the Bomb", "The Big Lebowski", "Seven Samurai", "The Truman Show", "Expelled: No Intelligence Allowed", "Inception", "The Ambassador", "Twenty Feet from Stardom", "L.A. Story", "Forks Over Knives", "GasLand", "Dean Spanley", "The Thin Blue Line", "Independence Day", "V for Vendetta", "Batman Unmasked", "The Usual Suspects", "The Protector", "The Wrestler", "L.A. Confidential", "The Shawshank Redemption", "Apocalypse Now", "The Wolverine", "The Fall", "Black Hawk Down", "Uncle Boonmee Who Can Recall His Past Lives", "Undefeated", "The True Story of Blackhawk Down", "The Jerk", "Harakiri", "Scarface", "Life of Pi", "The Avengers", "Serenity", "Snatch", "Inside Man", "The Dark Knight Rises", "District 9", "Roger & Me", "Ong-Bak: The Thai Warrior", "The Thin Red Line", "Saving Private Ryan", "Super Size Me", "Margin Call", "In Bruges", "The Blues Brothers", "Red Cliff", "Red Cliff II", "Jeff, Who Lives at Home", "This Is Spinal Tap", "Flash Point", "The Fifth Element", "Bowfinger", "Flight", "21 Jump Street", "Seven Psychopaths", "Ghostbusters", "Good Will Hunting", "Run Silent Run Deep", "$5 a Day", "Slumdog Millionaire", "Star Wars: Episode IV - A New Hope", "Remember the Titans", "Groundhog Day", "Ip Man", "Gandhi", "Good Night, and Good Luck.", "Chinatown", "The Boondock Saints", "Fair Game", "Kiss Kiss Bang Bang", "Superbad", "21", "Memento", "The Hangover", "Anchorman: The Legend of Ron Burgundy", "Lock, Stock and Two Smoking Barrels", "Twelve Monkeys", "Reservoir Dogs", "Being There", "Armageddon", "Source Code", "Primer", "Jaws", "Gran Torino", "The Princess Bride", "Kick-Ass", "A Fistful of Dollars", "Star Trek", "Jurassic Park", "Pulp Fiction", "The Good, the Bad and the Ugly", "The Lord of the Rings: The Return of the King", "Se7en", "Frontline", "Episode: A Class Divided", "The Prestige", "Ace Ventura: Pet Detective", "Ferris Bueller's Day Off", "Tombstone", "Hot Fuzz", "Shaun of the Dead", "Zoolander", "Get Him to the Greek", "The Royal Tenenbaums", "Hot Tub Time Machine", "Blazing Saddles", "Kung Fu Hustle", "Robin Hood: Men in Tights", "Zombieland", "Young Frankenstein", "Caddyshack", "Up in Smoke", "Duck, You Sucker", "Das Boot", "Starship Troopers", "Full Metal Jacket", "Captain America: The First Avenger", "Akira", "Batman Begins", "Mud", "Spirited Away", "The Breakfast Club", "Argo", "One Flew Over the Cuckoo's Nest", "Goodfellas", "Back to the Future", "Trainspotting", "Yojimbo", "Mrs. Carey's Concert", "Fiddler on the Roof", "Coffee and Cigarettes", "42", "The King of Kong: A Fistful of Quarters", "Death at a Funeral", "Lord of War", "Léon: The Professional", "True Grit", "Redemption", "X-Men: First Class", "X-Men", "Murderball", "Lee Daniels' The Butler", "The Fog of War: Eleven Lessons from the Life of Robert S. McNamara", "The Milagro Beanfield War", "The Maltese Falcon", "Smoke Signals", "Whale Rider", "The Lego Movie", "The Brothers Bloom", "Side by Side", "Midnight in Paris", "Selma" };

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // cand se incarca formularul, populam lista Watchlist cu fileme de mai sus
            populateWatchList();
        }

        // metoda pentru popularea listei Watchlist
        // am ales sa mutam logica intr-o metoda separata
        // deoarece facem acest lucru de mai multe ori, din locuri diferite in cod
        private void populateWatchList()
        {
            // golim list
            lstWatchlist.Items.Clear();

            // adaugam elementele
            for (int i = 0; i < movies.Length; i++)
            {
                lstWatchlist.Items.Add(movies[i]);
            }
        }

        // mutam filmele in lista din partea dreapta
        private void btnMoveToWatched_Click(object sender, EventArgs e)
        {
            // aici vom stocam elementele ce urmeaza sa le stergem
            // dupa ce mutam elementele selectate
            string[] toRemove = new string[lstWatchlist.SelectedItems.Count];

            for (int i = 0; i < lstWatchlist.SelectedItems.Count; i++)
            {
                // adaugam in lista Watched
                lstWatched.Items.Add(lstWatchlist.SelectedItems[i]);

                // adaugam in vectorul cu elemente ce urmeaza a fi sterse
                toRemove[i] = lstWatchlist.SelectedItems[i].ToString();
            }


            // stergem elementele selectate
            for (int i = 0; i < toRemove.Length; i++)
            {
                lstWatchlist.Items.Remove(toRemove[i]);
            }
        }

        private void btnSearchMovie_Click(object sender, EventArgs e)
        {
            string toSearch = txtSearchMovie.Text.ToLower();

            searchMovies(toSearch);
        }

        private void btnResetList_Click(object sender, EventArgs e)
        {
            txtSearchMovie.Clear();
            populateWatchList();
        }

        private void searchKeyPress(object sender, KeyPressEventArgs e)
        {
            // cand apasam ENTER cautam
            if (e.KeyChar == (char) 13)
            {
                searchMovies(((TextBox)sender).Text.ToLower());
            }
        }

        // deoarece cautam si la apasarea tastei ENTER si la click pe buton
        // mutam logica intr-o metoda separata
        private void searchMovies(string token)
        {
            lstWatchlist.Items.Clear();

            for (int i = 0; i < movies.Length; i++)
            {
                if (movies[i].ToLower().Contains(token) == true)
                    lstWatchlist.Items.Add(movies[i]);
            }
        }
    }
}
